# Mega Cable Tester Firmware #

This repo contains firmware for the Mega Cable Tester (the Mini Cable Tester firmware is in a separate repo).  Generally you should not need this repo unless you are building a new cable tester or replacing the processor; the normal control software is located elsewhere.

To install the firmware, simply plug in the cable tester and run "make" from within the "main-folder" directory on a Linux system (tested with Ubuntu 20.04).  Depending on your hardware permissions, you may need to run "sudo make" instead.  A window should pop up and show the flash progress, which will only take a few seconds.

On a new megatester, make sure to populate jumper J8 which should be set to 2.5V (bridging pins 1 and 2). 