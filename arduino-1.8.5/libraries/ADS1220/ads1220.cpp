
#include "ads1220.h"

ADS1220::ADS1220(int pin_cs, int pin_drdy)
{
	this->gain = 0;
	this->pin_cs = pin_cs;
	this->pin_drdy = pin_drdy;
	pinMode(this->pin_drdy, INPUT);
	pinMode(this->pin_cs, OUTPUT);
	digitalWrite(this->pin_cs, HIGH);
}

void ADS1220::attach_interrupt(void (*isr)())
{
	attachInterrupt(this->pin_drdy, isr, FALLING);
}

void ADS1220::spi_transaction_begin()
{
	SPI.beginTransaction(SPISettings(ADS1220_SPI_SPEED, ADS1220_SPI_ORDER, ADS1220_SPI_MODE));
	digitalWrite(this->pin_cs, LOW);
	delayMicroseconds(10);  // Takes ~8 us for the pin to settle
}

void ADS1220::spi_transaction_end()
{
	digitalWrite(this->pin_cs, HIGH);
	SPI.endTransaction();
	delayMicroseconds(10);
}

void ADS1220::reg_write_conf0(uint8_t mux, uint8_t gain, uint8_t pga)
{
	this->gain = gain;
	this->reg_write(0, ((mux & ADS1220_BITMASK_MUX) << ADS1220_BITSHIFT_MUX) |
					   ((gain & ADS1220_BITMASK_GAIN) << ADS1220_BITSHIFT_GAIN) |
					   ((pga & ADS1220_BITMASK_PGA) << ADS1220_BITSHIFT_PGA));
}

void ADS1220::reg_write_conf1(uint8_t dr, uint8_t mode, uint8_t cm, uint8_t ts, uint8_t bcs)
{
	this->reg_write(1, ((dr & ADS1220_BITMASK_DR) << ADS1220_BITSHIFT_DR) |
					   ((mode & ADS1220_BITMASK_MODE) << ADS1220_BITSHIFT_MODE) |
					   ((cm & ADS1220_BITMASK_CM) << ADS1220_BITSHIFT_CM) |
					   ((ts & ADS1220_BITMASK_TS) << ADS1220_BITSHIFT_TS) |
					   ((bcs & ADS1220_BITMASK_BCS) << ADS1220_BITSHIFT_BCS));
}

void ADS1220::reg_write_conf2(uint8_t vref, uint8_t filter, uint8_t psw, uint8_t idac)
{
	this->reg_write(2, ((vref & ADS1220_BITMASK_VREF) << ADS1220_BITSHIFT_VREF) |
					   ((filter & ADS1220_BITMASK_FILTER) << ADS1220_BITSHIFT_FILTER) |
					   ((psw & ADS1220_BITMASK_PSW) << ADS1220_BITSHIFT_PSW) |
					   ((idac & ADS1220_BITMASK_IDAC) << ADS1220_BITSHIFT_IDAC));
}

uint8_t ADS1220::reg_read(uint8_t id)
{	
	SPI.transfer(ADS1220_CMD_RREG_ONE(id & 3));
	uint8_t to_return = SPI.transfer(ADS1220_CMD_NULL);

	return to_return;
}

void ADS1220::reg_write(uint8_t id, uint8_t val)
{	
	SPI.transfer(ADS1220_CMD_WREG_ONE(id & 3));
	SPI.transfer(val);
}

void ADS1220::reset()
{
	SPI.transfer(ADS1220_CMD_RESET);
}

void ADS1220::data_start()
{
	SPI.transfer(ADS1220_CMD_START);
}

void ADS1220::wait_for_data()
{
	// drdy falls low for one spi clock cycle when data is ready
	// (it doesn't stay low)
	while (digitalRead(this->pin_drdy) == HIGH) {}
}

int32_t ADS1220::data_read()
{
	uint8_t msb = SPI.transfer(0);
	uint8_t midb = SPI.transfer(0);
	uint8_t lsb = SPI.transfer(0);
	
	// Pack the raw 24 bits we read into the upper bits of a uint32_t,
	// then reinterpret the result as a 2's compliment number.
	uint32_t result_unsigned = (msb << 24) | (midb << 16) | (lsb << 8);
	int32_t result = *(int32_t*)(&result_unsigned);
	
	// Remove the lower empty bits
	return (result >> 8);
}

//~ // Integer math version
//~ int32_t ADS1220::data_to_nv(int32_t data)
//~ {
	//~ // We need headroom for the computation, but result will fit in 32 bits
	//~ int64_t result = (int64_t) data;
	//~ // raw * 2.048V * 1e9 / ((2^(24-1))*(2^gain));
	//~ result = (result*15625L)>>(this->gain + 6);
	
	//~ return (int32_t)result;
//~ }

//~ // Integer math version
//~ int32_t ADS1220::data_to_mohms(int32_t data)
//~ {
	//~ // We need headroom for the computation, but result will fit in 32 bits
	//~ int64_t result = (int64_t) data;
	//~ // I am hardcoding the value of r_ref so I can pre-compute this.
	//~ // raw * ADS1220_R_REF ohms * 1e3 / ((2^(24-1))*(2^gain));
	//~ result = (result*390625L)>>(this->gain + 15);
	
	//~ return (int32_t)result;
//~ }

float ADS1220::data_to_nv(int32_t data)
{
	return ((float)data * 2.048 * 1.0e9) / (1 << (ADS1220_NUM_BITS - 1 + this->gain));
}

float ADS1220::data_to_ohms(int32_t data)
{
	return ((float)data * ADS1220_R_REF) / (1 << (ADS1220_NUM_BITS - 1 + this->gain));
}

