#ifndef ADS1220_h
#define ADS1220_h

#include <Arduino.h>
#include <SPI.h>

#define ADS1220_CMD_NULL (0)
#define ADS1220_CMD_RESET (0b00000111)
#define ADS1220_CMD_START (0b00001000)
#define ADS1220_CMD_SLEEP (0b00000011)
#define ADS1220_CMD_RDATA (0b00010000)  // Read data
#define ADS1220_CMD_RREG_ONE(r) (0b00100000 | ((r)<<2))  // r is the id, 0 to 3
#define ADS1220_CMD_RREG_ALL (0b00100011)
#define ADS1220_CMD_WREG_ONE(r) (0b01000000 | ((r)<<2))  // r is the id, 0 to 3
#define ADS1220_CMD_WREG_ALL (0b01000011)

#define ADS1220_SPI_SPEED (5000000)
#define ADS1220_SPI_ORDER (MSBFIRST)
#define ADS1220_SPI_MODE (SPI_MODE1)

#define ADS1220_BITMASK_MUX (0b1111)
#define ADS1220_BITSHIFT_MUX (4)
#define ADS1220_MUX_01 (0b0000)
#define ADS1220_MUX_02 (0b0001)
#define ADS1220_MUX_03 (0b0010)
#define ADS1220_MUX_12 (0b0011)
#define ADS1220_MUX_13 (0b0100)
#define ADS1220_MUX_23 (0b0101)
#define ADS1220_MUX_10 (0b0110)
#define ADS1220_MUX_32 (0b0111)
#define ADS1220_MUX_0G (0b1000)  // Must disable PGA, can use gains of 1, 2, 4
#define ADS1220_MUX_1G (0b1001)  // Must disable PGA, can use gains of 1, 2, 4
#define ADS1220_MUX_2G (0b1010)  // Must disable PGA, can use gains of 1, 2, 4
#define ADS1220_MUX_3G (0b1011)  // Must disable PGA, can use gains of 1, 2, 4
#define ADS1220_MUX_REF (0b1100)   // External reference, not internal
#define ADS1220_MUX_AVDD (0b1101)  // (AVDD - AVSS) / 4, PGA off
#define ADS1220_MUX_SHORT (0b1110) // Both shorted to (AVDD + AVSS) / 2

#define ADS1220_BITMASK_GAIN (0b111)
#define ADS1220_BITSHIFT_GAIN (1)
#define ADS1220_GAIN_1   (0b000)
#define ADS1220_GAIN_2   (0b001)
#define ADS1220_GAIN_4   (0b010)
#define ADS1220_GAIN_8   (0b011)
#define ADS1220_GAIN_16  (0b100)
#define ADS1220_GAIN_32  (0b101)
#define ADS1220_GAIN_64  (0b110)
#define ADS1220_GAIN_128 (0b111)
#define ADS1220_GAIN_MAX (ADS1220_GAIN_128)
#define ADS1220_GAIN_MIN (ADS1220_GAIN_1)
#define ADS1220_NUM_GAINS (ADS1220_GAIN_MAX + 1)

#define ADS1220_BITMASK_PGA (1)
#define ADS1220_BITSHIFT_PGA (0)
#define ADS1220_PGA_ON (0)
#define ADS1220_PGA_OFF (1)

#define ADS1220_BITMASK_DR (0b111)
#define ADS1220_BITSHIFT_DR (5)
#define ADS1220_DR_20 (0b000)
#define ADS1220_DR_45 (0b001)
#define ADS1220_DR_90 (0b010)
#define ADS1220_DR_175 (0b011)
#define ADS1220_DR_330 (0b100)
#define ADS1220_DR_600 (0b101)
#define ADS1220_DR_1000 (0b110)

#define ADS1220_BITMASK_MODE (0b11)
#define ADS1220_BITSHIFT_MODE (3)
#define ADS1220_MODE_NORMAL (0b00) // 1x data rate
#define ADS1220_MODE_DUTYCYCLE (0b01) // 0.25x
#define ADS1220_MODE_TURBO (0b10) // 2x

#define ADS1220_BITMASK_CM (1)
#define ADS1220_BITSHIFT_CM (2)
#define ADS1220_CM_SINGLE (0)
#define ADS1220_CM_CONTINUOUS (1)

#define ADS1220_BITMASK_TS (1)
#define ADS1220_BITSHIFT_TS (1)
#define ADS1220_TS_OFF (0)
#define ADS1220_TS_ON (1)

#define ADS1220_BITMASK_BCS (1)
#define ADS1220_BITSHIFT_BCS (0)
#define ADS1220_BCS_OFF (0)
#define ADS1220_BCS_ON (1)

#define ADS1220_BITMASK_VREF (0b11)
#define ADS1220_BITSHIFT_VREF (6)
#define ADS1220_VREF_INTERNAL (0b00)
#define ADS1220_VREF_REF0 (0b01)
#define ADS1220_VREF_REF1 (0b10)
#define ADS1220_VREF_SUPPLY (0b11)

#define ADS1220_BITMASK_FILTER (0b11)
#define ADS1220_BITSHIFT_FILTER (4)
#define ADS1220_FILTER_NONE (0b00)
#define ADS1220_FILTER_BOTH (0b01)
#define ADS1220_FILTER_50HZ (0b10)
#define ADS1220_FILTER_60HZ (0b11)

#define ADS1220_BITMASK_PSW (0b1)
#define ADS1220_BITSHIFT_PSW (3)
#define ADS1220_PSW_OPEN (0b00)
#define ADS1220_PSW_AUTO (0b01)

#define ADS1220_BITMASK_IDAC (0b111)
#define ADS1220_BITSHIFT_IDAC (0)
#define ADS1220_IDAC_OFF (0b000)

#define ADS1220_NUM_BITS (24)
#define ADS1220_VAL_FS (1 << (ADS1220_NUM_BITS-1))
#define ADS1220_VAL_UNDERFLOW ((ADS1220_VAL_FS * 4L) / 10L)
#define ADS1220_VAL_OVERFLOW ((ADS1220_VAL_FS * 95L) / 100L)  

#define ADS1220_CHIPTEMP_CALIB_CELSIUS (0.03125 / (1<<(ADS1220_NUM_BITS-14)))

#ifndef ADS1220_R_REF
	#define ADS1220_R_REF 2000000L // Ohms
#endif

class ADS1220
{
	public:
		ADS1220(int pin_cs, int pin_drdy);
		void spi_transaction_begin();  // Enables CS and begins a spi transaction.
									   // Must be in a transaction to read and
									   // write data to the device.
		void spi_transaction_end();  // Disables CS and ends a spi transaction
		void reg_write_conf0(uint8_t mux, uint8_t gain, uint8_t pga);
		void reg_write_conf1(uint8_t dr, uint8_t mode, uint8_t cm, uint8_t ts, uint8_t bcs);
		void reg_write_conf2(uint8_t vref, uint8_t filter, uint8_t psw, uint8_t idac);
		//void reg_write_conf3(uint8_t i1mux, uint8_t i2mux, uint8_t drdym);
		void data_start();  // Starts measurements
		void reset();  // Reset to defaults
		int32_t data_read();  // Read in data
		float data_to_nv(int32_t data);  // Convert ADC code to nanovolts assuming the internal reference is used
		float data_to_ohms(int32_t data);  // Convert ADC code to ohms assuming an external reference series resistor is used
		//~ int32_t data_to_nv(int32_t data);  
		//~ int32_t data_to_mohms(int32_t data);
		void wait_for_data();  // Block until data is ready
		void attach_interrupt(void (*isr)());
	protected:
		void reg_write(uint8_t id, uint8_t val);
		uint8_t reg_read(uint8_t id);
		int pin_cs;
		int pin_drdy;
		uint8_t gain;
};

#endif
