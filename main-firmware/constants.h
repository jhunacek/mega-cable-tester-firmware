#pragma once

#include "ads1220.h"

#define PIN_LED_BOOT (17)
#define PIN_LED_RUN (18)
#define PIN_LED_ERR (19)
#define PIN_CS_ADC (10)
#define PIN_DATA_READY (14)
#define PIN_MUXA_EN (24)
#define PIN_MUXB_EN (35)

#define N_PROGRESS 4
extern const uint8_t PIN_LED_PROGRESS[N_PROGRESS];

#define NUM_GAINS_USED 3
extern const uint8_t GAINS_USED[NUM_GAINS_USED];

#define NUM_MUX_BITS (10)
extern const uint8_t MUX_A_PINS[NUM_MUX_BITS];
extern const uint8_t MUX_B_PINS[NUM_MUX_BITS];

#define NUM_PINS (1024)

#define ADC_TARGET_MUX (ADS1220_MUX_10)
#define ADC_TARGET_CURRENT (ADS1220_MUX_23)

// Value of R12
#define CURRENT_CALIB_RESISTOR (1000L)

#define CALIB_REPEAT 10

#define MODE_OPENS 0
#define MODE_CALIB 1
#define MODE_MAIN 2