#pragma once

#include "Arduino.h"

// Fill in some missing functions needed for std::vector
// https://forum.pjrc.com/threads/23467-Using-std-vector?p=69787&viewfull=1#post69787
namespace std 
{
  void __throw_bad_alloc()
  {
    Serial.println("Unable to allocate memory");
    while(1){}
  }
  
  void __throw_invalid_argument(char const*)
  {
    Serial.println("Invalid argument");
    while(1){}
  }
  
  void __throw_logic_error(char const*)
  {
    Serial.println("Logic error");
    while(1){}
  }

  void __throw_length_error( char const*e )
  {
    Serial.print("Length Error :");
    Serial.println(e);
    while(1){}
  }
}