#include "constants.h"

const uint8_t PIN_LED_PROGRESS[4] = {20, 21, 22, 23};

const uint8_t GAINS_USED[NUM_GAINS_USED] = {0, 4, 7};

const uint8_t MUX_A_PINS[NUM_MUX_BITS] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
const uint8_t MUX_B_PINS[NUM_MUX_BITS] = {25, 26, 27, 28, 29, 30, 31, 32, 33, 34};
