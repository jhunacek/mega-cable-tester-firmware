#include <Arduino.h>
#include <SPI.h>
#include <Streaming.h>
#include <vector>
#include "names.h"
#include "constants.h"
#include "ads1220.h"

using namespace std;

ADS1220 adc(PIN_CS_ADC, PIN_DATA_READY);

// False when we finish a pin combo
vector< vector<bool> > check_again(NUM_PINS, vector<bool>(NUM_PINS));

// False when we need to skip a pin
vector<bool> use_pin(NUM_PINS, false);

// State for monitoring ground shorts
vector<bool> gnd_short(NUM_PINS, false);
vector<bool> check_gnd_short(NUM_PINS, true);

// Calibration
float current_nA = 0;
float offset_ohms = 0;

// Run N times slower (more accurate if there is significant parasitic capacitance)
int speed_factor = 1; // Normal mode
//~ int speed_factor = 3; // Slow mode

// Change multiplexer target
void set_mux(const uint8_t* pins, uint16_t output)
{
	for (int i = 0; i < NUM_MUX_BITS; i++)
	{
		digitalWrite(pins[i], (output >> i) & 1);
	}
}
#define set_mux_a(output) (set_mux(MUX_A_PINS, output))
#define set_mux_b(output) (set_mux(MUX_B_PINS, output))

// Change ADC target and gain
void set_adc(uint8_t target, uint8_t gain)
{
	adc.reg_write_conf0(target, gain, ADS1220_PGA_ON);
	adc.data_start();
	delay(200 * speed_factor);  // Let it settle
}

// Switch between the internal and external reference voltage
void adc_ref_external(bool use_external)
{
	if (use_external)
		adc.reg_write_conf2(ADS1220_VREF_REF0, ADS1220_FILTER_NONE, ADS1220_PSW_OPEN, ADS1220_IDAC_OFF);
	else
		adc.reg_write_conf2(ADS1220_VREF_INTERNAL, ADS1220_FILTER_NONE, ADS1220_PSW_OPEN, ADS1220_IDAC_OFF);
}

// Store calibration results
void calibrate_adc()
{
	// Short the output so we know current is flowing
	set_mux_a(PIN_GND_CHECK);
	set_mux_b(PIN_GND_CHECK);
	
	// Read the voltage across the fixed resistor
	// to measure the current
	adc.spi_transaction_begin();
	adc_ref_external(false);
	set_adc(ADC_TARGET_CURRENT, ADS1220_GAIN_128);
	current_nA = 0;
	for (int i = 0; i < CALIB_REPEAT; i++)
	{
		adc.data_start();
		adc.wait_for_data();
		int32_t result_nV = adc.data_to_nv(adc.data_read());
		current_nA += result_nV / CURRENT_CALIB_RESISTOR;
	}
	current_nA /= CALIB_REPEAT;
	adc.spi_transaction_end();
		
	// Read the value for a shorted output to find
	// the resistance of the mux chips.
	adc.spi_transaction_begin();
	adc_ref_external(true);
	set_adc(ADC_TARGET_MUX, ADS1220_GAIN_128);
	offset_ohms = 0;
	for (int i = 0; i < CALIB_REPEAT; i++)
	{
		adc.data_start();
		adc.wait_for_data();
		offset_ohms += adc.data_to_ohms(-adc.data_read());
	}
	offset_ohms /= CALIB_REPEAT;
	adc.spi_transaction_end();
}

void setup() 
{	
	pinMode(PIN_MUXA_EN, OUTPUT);
	pinMode(PIN_MUXB_EN, OUTPUT);
	pinMode(PIN_LED_BOOT, OUTPUT);
	pinMode(PIN_LED_RUN, OUTPUT);
	pinMode(PIN_LED_ERR, OUTPUT);
	pinMode(PIN_CS_ADC, OUTPUT);
	pinMode(PIN_DATA_READY, INPUT);
	for (int i = 0; i < N_PROGRESS; i++)
		pinMode(PIN_LED_PROGRESS[i], OUTPUT);
	
	digitalWrite(PIN_MUXA_EN, LOW);
	digitalWrite(PIN_MUXB_EN, LOW);
	digitalWrite(PIN_LED_BOOT, HIGH);
	digitalWrite(PIN_CS_ADC, HIGH);

	for (uint8_t i = 0; i < NUM_MUX_BITS; i++)
	{
		pinMode(MUX_A_PINS[i], OUTPUT);
		pinMode(MUX_B_PINS[i], OUTPUT);
	}
	
	// Wait for the PC
	while (!Serial)	{}
	
	Serial.begin(9600); // Teensy USB is always 12 Mbit/sec
	SPI.begin();
	
	// Initialize the ADC (ADS1220_DR_1000 is 2kHz in Turbo mode)
	adc.spi_transaction_begin();
	adc.reg_write_conf1(ADS1220_DR_1000, ADS1220_MODE_TURBO, ADS1220_CM_SINGLE, ADS1220_TS_OFF, ADS1220_BCS_OFF);
	adc_ref_external(true);
	adc.spi_transaction_end();

	calibrate_adc();
	
	
}

// Ground short checks
void run_ground_shorts(uint8_t mode, float (*data_convert)(int32_t), float data_lim, int gain)
{
	for (uint16_t bbb = 0; bbb < NUM_PINS; bbb++)
	{
		if (!use_pin[bbb] || !check_gnd_short[bbb])
			continue;
		
		// Check for a reboot signal
		if (Serial.available() && Serial.read() == 'r')
			return;
		
		set_mux_b(bbb);
		set_mux_a(PIN_GND_CHECK);
		
		// Perform a slow (accurate) reading right away, there
		// aren't many of these so it's ok
		delay(10 * speed_factor);         
		adc.data_start();
		adc.wait_for_data();
		int32_t result_raw = -adc.data_read();
			
		if ((result_raw <= data_lim) && (result_raw >= -data_lim))
		{
			if (mode == MODE_MAIN)
			{
				float result = data_convert(result_raw) - offset_ohms;
				Serial << "GND," << pin_name[bbb] << "," << result << endl;
			}
			gnd_short[bbb] = true;
			continue;
		}

		// If we made it here then we must be out of range.  Mark the gain
		// as found so we don't keep trying.
		check_gnd_short[bbb] = false;
		
		// If we are out of range on the lowest gain choice then
		// we call it infinite and move on with life
		if (mode == MODE_OPENS || (mode == MODE_MAIN && gain == 0))
		{ 
			if (result_raw > data_lim)
				Serial << "GND," << pin_name[bbb] << "," << "+inf" << endl;
			else if (result_raw < -data_lim)
				Serial << "GND," << pin_name[bbb] << "," << "-inf" << endl;
		}    
	}
}

// Normal pin combo checks
void run_normal(uint8_t mode, float (*data_convert)(int32_t), float data_lim, bool full_matrix, int gain)
{
	// Allow a looser data limit requirement on the first quick read, 
	// use regular limit for final read
	float data_lim_loose = data_lim * 1.4;
	
	for (uint16_t bbb = 0; bbb < NUM_PINS; bbb++)
	{
		if (!use_pin[bbb])
			continue;
			
		// Check for a reboot signal
		if (Serial.available() && Serial.read() == 'r')
			return;
			
		set_mux_b(bbb);		
		
		// Ground shorts cause fake shorts to all other pins (which slows
		// things down), but we still want to check in case there's a smaller
		// cross-short.  (Ex: 1M ground short, but 1k cross short).
		// However, we shouldn't check calibration for these crossshorts
		// because they're going to be bad.  The standard calibration
		// will get us within 50%, which is good enough under the circumstances.
		// Note: for two pins with no cross short, a ground short
		// on the low side doesn't cause a ghost.  I could reverse the
		// mux in these cases to reduce ghosting.  This is why some
		// combos do not show ghosts already.
		if (gnd_short[bbb] && mode == MODE_CALIB)
			continue;
		
		for (uint16_t aaa = 0; aaa < NUM_PINS; aaa++)
		{   
			if (!use_pin[aaa])
				continue;
			
			// See above
			if (gnd_short[aaa] && mode == MODE_CALIB)
				continue; 
				
			// Don't try again if we already were out of range
			// for a previous gain choice
			if (!check_again[aaa][bbb])
				continue;
			
			set_mux_a(aaa);
			
			// Give the pins a moment to settle and take an initial 
			// reading.  This sleep plus the 500 us read time sets
			// the overall speed for measuring a sparse matrix.		
			// Using a value that is two small can cause you to miss
			// high-impedance (~MOhm) elements measured directly after
			// an open.	
			delayMicroseconds(200 * speed_factor);
			adc.data_start();
			adc.wait_for_data();
			int32_t result_raw = -adc.data_read();
			
			// If we didn't go way out of range, then let's let it settle and read
			// for real this time.
			if ((result_raw <= data_lim_loose) && (result_raw >= -data_lim_loose))
			{
				// Delay much longer for the pin to settle, since
				// we may not have moved from an open to an open.
				delay(10 * speed_factor);         
				adc.data_start();
				adc.wait_for_data();
				result_raw = -adc.data_read();
				
				// Now check if we are actually in range
				if ((result_raw <= data_lim) && (result_raw >= -data_lim))
				{
					
					if (mode == MODE_MAIN)
					{
						float result = data_convert(result_raw) - offset_ohms;
						Serial << pin_name[aaa] << "," << pin_name[bbb] << "," << result << endl;
					}
					else if (mode == MODE_CALIB)
					{
						float result = data_convert(-result_raw) / CURRENT_CALIB_RESISTOR;
						Serial << "icor," << pin_name[aaa] << "," << pin_name[bbb] << "," << _FLOAT(result,4) << endl;
					}
					continue;
				}
				//~ else
				//~ {
					//~ if (mode == MODE_OPENS)
						//~ Serial << "debug," << "unneeded open re-check," << aaa << "," << bbb << endl;
				//~ }
			}

			// If we made it here then we must be out of range.  Mark the gain
			// as found so we don't keep trying.
			check_again[aaa][bbb]= false;

			// If we are out of range on the lowest gain choice then
			// we call it infinite and move on with life
			if (mode == MODE_OPENS || (mode == MODE_MAIN && gain == 0))
			{ 
				if (result_raw > data_lim)
					Serial << pin_name[aaa] << "," << pin_name[bbb] << "," << "+inf" << endl;
				else if (result_raw < -data_lim)
					Serial << pin_name[aaa] << "," << pin_name[bbb] << "," << "-inf" << endl;
			}          
		}    
	}
}

void loop() 
{  
	// Reset state
	digitalWrite(PIN_LED_RUN, LOW);	
	for (uint16_t i = 0; i < NUM_PINS; i++)
	{
		gnd_short[i] = false;
		check_gnd_short[i] = true;
		for (uint16_t j = 0; j < NUM_PINS; j++)
			check_again[i][j] = true;
	}
	
	// Signal that we are ready
	Serial << "booted" << endl;
	
	// Wait for a start signal
	while (!(Serial.available()) || Serial.read() != 's') {}
	
	// Read config
	while (!(Serial.available())) {}
	bool full_matrix = (bool)Serial.read();
	while (!(Serial.available())) {}
	bool target_self = (bool)Serial.read();  // For old cable tester
	while (!(Serial.available())) {}
	bool block_diag = (bool)Serial.read();
	while (!(Serial.available())) {}
	int num_run_a = Serial.read() - '0';
	while (!(Serial.available())) {}
	int num_run_b = Serial.read() - '0';
	
	//~ Serial << "debug," << num_run_a << "," << num_run_b << endl;
	
	// Decide what pins to use
	for (int i = 0; i < NUM_PINS; i++)
	{
		// Default to everything
		use_pin[i] = true;
		
		// Skip NC pins
		if (strcmp(pin_name[i], "NC") == 0)
			use_pin[i] = false;
			
		// Use the first num_run_a A connectors
		if (pin_name[i][0] == 'A')
		{
			int num = pin_name[i][1] - '0';
			if (num >= num_run_a)
				use_pin[i] = false;
		}
		
		// Use the first num_run_b B connectors
		if (pin_name[i][0] == 'B')
		{
			int num = pin_name[i][1] - '0';
			if (num >= num_run_b)
				use_pin[i] = false;
		}
	}
	
	// If full_matrix is false, only check half of the matrix
	if (!full_matrix)
	{
		for (uint16_t bbb = 0; bbb < NUM_PINS; bbb++)
		{		
			for (uint16_t aaa = 0; aaa < (bbb+1); aaa++)
			{   
				check_again[aaa][bbb] = false;	
			}
		}
	}
	
	// If block_diag is true, only check for shorts within a connector
	if (block_diag)
	{
		for (uint16_t bbb = 0; bbb < NUM_PINS; bbb++)
		{		
			for (uint16_t aaa = 0; aaa < NUM_PINS; aaa++)
			{   
				if ((pin_name[aaa][0] != pin_name[bbb][0]) || (pin_name[aaa][1] != pin_name[bbb][1]))
					check_again[aaa][bbb] = false;	
			}
		}
	}
	
	digitalWrite(PIN_LED_RUN, HIGH);
	
	int start_time = millis();
	
	adc.spi_transaction_begin();

	Serial << "calib," << current_nA << "," << offset_ohms << ",mega" << endl;
	
	uint8_t gain;
	float (*data_convert)(int32_t data);
	float data_lim;
	
	// First check for opens using the voltage reference (the series
	// resistance won't have a voltage drop to act as a reference).
	// Then do everything with the series resistor as refeference.
	gain = 0;
	adc_ref_external(false);
	data_convert = [](int32_t data) {return adc.data_to_nv(data);};
	// FS/2 means R_measured = R_monitor = 2 Mohm
	data_lim = ADS1220_VAL_FS * 0.5 * .95; 
	set_adc(ADC_TARGET_MUX, gain);
	
	// Check for existance (not value yet) of ground shorts
	Serial << "gain," << gain << "," << "open" << endl;
	run_ground_shorts(MODE_OPENS, data_convert, data_lim, gain);
	
	// If we are doing a half matrix, we can swap the mux to put ground
	// shorts on ground side, greatly reducing the impact of ghosting
	if (!full_matrix)
	{
		for (uint16_t bbb = 0; bbb < NUM_PINS; bbb++)
		{		
			// Check the upper half, the half we default to
			for (uint16_t aaa = bbb + 1; aaa < NUM_PINS; aaa++)
			{   
				if (check_again[aaa][bbb])
				{
					if (gnd_short[bbb] && !gnd_short[aaa])
					{
						check_again[aaa][bbb] = false;	
						check_again[bbb][aaa] = true;	
					}
				}	
			}
		}
	}
	
	// Run checks for opens.  This step takes the majority of the run
	// time for sparse matrices, we are limited by the 2 kHz ADC sample
	// rate and the fact that we have to use a settle time when muxing
	// between closed and open combos.
	run_normal(MODE_OPENS, data_convert, data_lim, full_matrix, gain);
	
	// Reset to use external reference series resistor
	adc_ref_external(true);
	
	// Do current correction measurements for closed channels.  Measure
	// the ratio of the 1k return resistor to the 2M output resistor,
	// which tells us about the leakage current for that pin combo.
	// We can't store the results, so correction is done in software.
	// This helps accuracy for > 0.5 Mohm resistances.
	gain = ADS1220_GAIN_128;
	data_convert = [](int32_t data) {return adc.data_to_ohms(data);};
	data_lim = ADS1220_VAL_OVERFLOW;
	set_adc(ADC_TARGET_CURRENT, gain);
	
	// Don't run the ground short one because it doesn't use the 
	// return resistor (we can't do this measurement for ground shorts)
	Serial << "gain," << gain << "," << "icor" << endl;
	run_normal(MODE_CALIB, data_convert, data_lim, full_matrix, gain);
	
	// Set up to run main measurements
	data_lim = ADS1220_VAL_OVERFLOW;
			
	// Scan through gains
	for (int16_t ggg = 0; ggg < NUM_GAINS_USED; ggg++)
	{
		gain = GAINS_USED[ggg];			
		set_adc(ADC_TARGET_MUX, gain);
		Serial << "gain," << gain << endl;
		
		run_ground_shorts(MODE_MAIN, data_convert, data_lim, ggg);
		run_normal(MODE_MAIN, data_convert, data_lim, full_matrix, ggg);
	}
	
	adc.spi_transaction_end();

	Serial << "time," << millis() - start_time << endl;
	digitalWrite(PIN_LED_RUN, LOW);
	delay(1000);

}


