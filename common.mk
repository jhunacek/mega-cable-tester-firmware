# Based on code from https://forum.pjrc.com/threads/25439-Makefile-Compatible-With-Arduino-IDE-Sketches
# All credit to the original authors.
# 2017-05-24 modified for Teensy 3.6

# Configurable options (remember to "make clean" everything if you change something)
OPTIONS = -DF_CPU=180000000 -DUSB_SERIAL -DLAYOUT_US_ENGLISH 

# Options needed by many Arduino libraries to configure for Teensy
OPTIONS += -D__MK66FX1M0__ -DARDUINO=181 -DTEENSYDUINO=134

#************************************************************************
# Location of Teensyduino utilities, Toolchain, and Arduino Libraries.
# To use this makefile without Arduino, copy the resources from these
# locations and edit the pathnames.  The rest of Arduino is not needed.
#************************************************************************

# Arduino/Teensy files, Core libs
ARDUINO_HW_HOME := $(ARDUINO_HOME)/hardware
USER_LIB_HOME := $(ARDUINO_HOME)/libraries
CORE_LIB_HOME := $(ARDUINO_HOME)/hardware/teensy/avr/libraries
TEENSY_HOME := $(ARDUINO_HW_HOME)/teensy/avr/cores/teensy3

# path location for Teensy Loader, teensy_post_compile and teensy_reboot
TOOLSPATH := $(ARDUINO_HW_HOME)/tools

# path location for the arm-none-eabi compiler
COMPILERPATH := $(ARDUINO_HW_HOME)/tools/arm/bin

# where to store build output files
OBJ_DIR := obj

#************************************************************************
# Library includes and sources
#************************************************************************

LIBRARY_PATHS := $(foreach lib,$(CORE_LIBRARIES),$(CORE_LIB_HOME)/$(lib)) $(foreach lib,$(USER_LIBRARIES),$(USER_LIB_HOME)/$(lib)) $(foreach lib,$(COMMON_CODE_FOLDERS),./$(lib))
LIBRARY_INCLUDES := $(foreach libdir,$(LIBRARY_PATHS),-I$(libdir))
LIBRARY_C_FILES := $(foreach libdir,$(LIBRARY_PATHS),$(wildcard $(libdir)/*.c))
LIBRARY_CPP_FILES := $(foreach libdir,$(LIBRARY_PATHS),$(wildcard $(libdir)/*.cpp))

VPATH := . $(LIBRARY_PATHS) $(TEENSY_HOME)

#************************************************************************
# Settings below this point usually do not need to be edited
#************************************************************************

# CPPFLAGS = compiler options for C and C++
CPPFLAGS := -Wall -g -Os -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -nostdlib -MMD -fdata-sections -ffunction-sections $(OPTIONS) -I. -I$(TEENSY_HOME) $(LIBRARY_INCLUDES)

# compiler options for C++ only
CXXFLAGS := -std=gnu++0x -felide-constructors -fno-exceptions -fno-rtti

# compiler options for C only
CFLAGS :=

# linker options
LDFLAGS := -Os -Wl,--gc-sections -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -fsingle-precision-constant -T$(TEENSY_HOME)/mk66fx1m0.ld

# additional libraries to link (order matters!)
LIBS := -larm_cortexM4lf_math -lm

# names for the compiler programs
CC := $(abspath $(COMPILERPATH))/arm-none-eabi-gcc
CXX := $(abspath $(COMPILERPATH))/arm-none-eabi-g++
OBJCOPY := $(abspath $(COMPILERPATH))/arm-none-eabi-objcopy
SIZE := $(abspath $(COMPILERPATH))/arm-none-eabi-size

# automatically create lists of the sources and objects
C_FILES := $(wildcard *.c) $(LIBRARY_C_FILES) $(wildcard $(TEENSY_HOME)/*.c)
CPP_FILES := $(wildcard *.cpp) $(LIBRARY_CPP_FILES) $(wildcard $(TEENSY_HOME)/*.cpp)
ABSOLUTE_OBJS := $(TARGET).o $(C_FILES:.c=.o) $(CPP_FILES:.cpp=.o)
OBJS := $(addprefix $(OBJ_DIR)/, $(notdir $(ABSOLUTE_OBJS)))
OBJ_DEPS := $(OBJS:.o=.d)

# the actual makefile rules (all .o files built by GNU make's default implicit rules)

all: $(OBJ_DIR) $(TARGET).hex upload
.PHONY: all

upload:
	@echo "Uploading..."
	@$(abspath $(TOOLSPATH))/teensy_post_compile -file=$(basename $(TARGET).hex) -path=$(shell pwd) -tools=$(abspath $(TOOLSPATH))
	@-$(abspath $(TOOLSPATH))/teensy_reboot

monitor:
	@echo "Opening minicom (Ctrl+A;x to exit, 'sudo minicom -s' to set up and save teensy config)..."
	@minicom teensy

mon: monitor

pymon:
	@echo "Opening python serial terminal..."
	@python -m serial.tools.miniterm /dev/ttyACM0 9600

$(TARGET).elf: $(OBJS) $(TEENSY_HOME)/mk66fx1m0.ld
	@echo "[LINK] $@"
	@$(CC) $(LDFLAGS) -o $@ $(OBJS) $(LIBS)

$(OBJ_DIR)/$(TARGET).o: $(TARGET).ino
	@echo "[INO] $<"
	@$(CXX) $(CPPFLAGS) $(CXXFLAGS) -o "$@" -c -x c++ -include Arduino.h "$<"

%.hex: %.elf
	$(SIZE) $<
	$(OBJCOPY) -O ihex -R .eeprom $< $@
	
$(OBJ_DIR)/%.o: %.c
	@echo "[CC] $<"
	@$(CC) -c $(CPPFLAGS) $(CFLAGS) -o "$@" "$<"

$(OBJ_DIR)/%.o: %.cpp
	@echo "[CXX] $<"
	@$(CXX) -c $(CPPFLAGS) $(CXXFLAGS) -o "$@" "$<"

$(OBJ_DIR):
	mkdir -p $(OBJ_DIR)

# compiler generated dependency info
-include $(OBJS:.o=.d)

clean:
	$(RM) -rf $(OBJ_DIR)
	$(RM) -f $(TARGET).elf $(TARGET).hex
.PHONY: clean
